from django.contrib import admin
from .models import Game, Location, Player, Task
from django.contrib.admin.models import LogEntry
from django.conf import settings

# Register your models here.
admin.site.site_header = 'Geolocation Game Admin Dashboard'
admin.site.site_url = "/admin"

@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ('location_name', 'latitude', 'longitude',)
    search_fields = ('location_name',)

    fieldsets = (
        (None, {
            'fields': ('location_name', 'latitude', 'longitude',)
        }),
    )

    class Media:
        if hasattr(settings, 'GOOGLE_MAPS_API_KEY') and settings.GOOGLE_MAPS_API_KEY:
            css = {
                'all': ('css/admin/location_picker.css',),
            }
            js = (
                'https://maps.googleapis.com/maps/api/js?key={}'.format(settings.GOOGLE_MAPS_API_KEY),
                'js/admin/location_picker.js',
            )

@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
      list_display = ('player_id', 'user',)
      ordering = ('player_id',)
      search_fields = ('player_id', 'user.username')


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
      list_display = ('game_id','game_name','get_players', 'get_tasks')
      ordering = ('game_id','game_name',)
      search_fields = ('game_id','game_name',)

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
      list_display = ('task_id', 'task_name', 'task_description', 'task_location', 'get_result')
      ordering = ('task_id', 'task_name',)
      search_fields = ('task_id', 'task_name',)
      exclude = ('result',)

LogEntry.objects.all().delete()
 