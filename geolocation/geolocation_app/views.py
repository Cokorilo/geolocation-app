from getpass import getuser
from http.client import HTTPResponse
from turtle import distance
from unicodedata import decimal
from django.shortcuts import render, redirect 
from .models import Game, Location, Task
import geocoder
import folium
from decimal import Decimal
import geopy.distance

# Create your views here.
WINNING_RADIUS = 50.0


def index(request):
    return render(request, 'registration/logins.html')


def select_game(request):
    context = {
        'game_list': Game.objects.all(),
    }
    return render(request,'map/choose_game.html', context)


def validate_game(request):

    if request.method == 'POST':
        game_name = request.POST.get('game_selected') 

        selected_game = get_current_game_data(game_name, request.user.username)

        dict = {
            'map': get_map_data(selected_game),
            'game_list': Game.objects.all(),
            'current_user' : request.user.username,
            'game': selected_game,
            'game_selected' : game_name
        }
        return render(request, 'map/gameboard.html', dict)   


def validate_location(request):

    if request.method == 'POST':
        game_name = request.POST.get('tag')
        latitude = request.POST.get('lat')
        longitude = request.POST.get('lng')

        current_game_data = get_current_game_data(game_name, request.user.username)    

        calculate_result(current_game_data, latitude, longitude, request)


        dict = {
            'map': get_new_map_data(current_game_data, latitude, longitude, request),
            'game_list': Game.objects.all(),
            'current_user' : str(request.user.username),
            'game': current_game_data,
            'game_selected' : game_name,
            'write_result' : write_result(current_game_data, request)
        }
        return render(request, 'map/gameboard.html', dict)   


def get_current_game_data(game_name, current_user):
    game_data = {}
    all_tasks = {}
    co_players = {}
    current_player = {}

    for game in Game.objects.all():
        if game.game_name == game_name:
            for game_task in game.game_tasks.all():
                all_tasks[game_task] = {}
                all_tasks[game_task]["task_id"] = game_task.task_id
                all_tasks[game_task]["task_name"] = game_task.task_name
                all_tasks[game_task]["task_description"] = game_task.task_description
                all_tasks[game_task]["result"] = [str(result) for result in game_task.result.all()]
                all_tasks[game_task]["task_location"] = {}
                all_tasks[game_task]["task_location"]['location_name'] = game_task.task_location.location_name
                all_tasks[game_task]["task_location"]['latitude'] = game_task.task_location.latitude
                all_tasks[game_task]["task_location"]['longitude'] = game_task.task_location.longitude
            for player in game.players.all():
                if player.user.username != current_user:
                    co_players[player] = {}
                    co_players[player]["player_id"] = player.player_id
                    co_players[player]["player_name"] = player.user.username
                else: 
                    current_player[player] = {}
                    current_player[player]["player_id"] = player.player_id
                    current_player[player]["player_name"] = player.user.username

    game_data["all_tasks"] = all_tasks
    game_data["co_players"] = co_players
    game_data["current_player"] = current_player

    return game_data


def get_map_data(selected_game):

    map = folium.Map(location=get_center_position(selected_game),zoom_start=13)
 

    for task in selected_game["all_tasks"]:

        folium.Marker(
            location = [task.task_location.latitude, task.task_location.longitude], icon=folium.Icon(color='green'),
            popup = task.task_name,
        ).add_to(map)

    map = map._repr_html_()


    return map


def get_new_map_data(selected_game, player_latitude, player_longitude, request):

    map = folium.Map(location=get_center_position(selected_game), zoom_start=13)
    
    for task in selected_game["all_tasks"]:

        task_location = [task.task_location.latitude, task.task_location.longitude]
        player_location = [player_latitude, player_longitude]

        if(calculate_distance(task_location, player_location)):
            folium.Marker(
                location = task_location, icon=folium.Icon(color='red'), 
                popup = task.task_name + " - REŠENO",
            ).add_to(map) 

        else:
            folium.Marker(
                location = task_location, icon=folium.Icon(color='green'),
                popup = task.task_name,
            ).add_to(map)
        folium.Marker(location = player_location, zoom_start=10, popup=request.user.username,).add_to(map)
    map = map._repr_html_()

    return map


def get_center_position(selected_game):
    all_task_locations = []

    for task in selected_game["all_tasks"]:
        all_task_locations.append([task.task_location.latitude, task.task_location.longitude])

    x = [p[0] for p in all_task_locations]
    y = [p[1] for p in all_task_locations]
    centroid = [sum(x) / len(all_task_locations), sum(y) / len(all_task_locations)]

    return centroid

     
def calculate_result(current_game_data, player_latitude, player_longitude, request):

      for task in current_game_data["all_tasks"]:
        
        task_location = [task.task_location.latitude, task.task_location.longitude]
        player_location = [Decimal(player_latitude), Decimal(player_longitude)]

    
        if(calculate_distance(task_location, player_location)):

            for player in current_game_data["current_player"]:
                if player.user.username == request.user.username:           
                    current_task = Task.objects.get(task_id = task.task_id)
                    current_task.result.add(player.player_id) 
         

def calculate_distance(task_location, player_location):
    
    location_distance = geopy.distance.geodesic(task_location, player_location).m

    if(location_distance < WINNING_RADIUS):
        return True
    return False


def write_result(current_game_data, request):

    for task in current_game_data["all_tasks"]:
        for player in task.result.all():
            if player.user.username == request.user.username:
                return "Rešili ste zadatak: " + task.task_name
            else:
                return ""

                