from pickle import TRUE
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import views as views_auth

# Create your models here.

class Location(models.Model):

    location_name = models.CharField(max_length=255)

    latitude = models.DecimalField(
                max_digits=9, decimal_places=6, null=True, blank=True)

    longitude = models.DecimalField(
                max_digits=9, decimal_places=6, null=True, blank=True)

    def __str__(self):
        return self.location_name
    

class Player(models.Model):

    player_id = models.AutoField('ID', primary_key=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username


class Task(models.Model):

     task_id = models.AutoField('ID', primary_key=True)
     task_name = models.CharField(max_length=100)
     task_description = models.CharField(max_length=1000)
     task_location = models.OneToOneField(Location, on_delete=models.CASCADE, blank=True)
     result = models.ManyToManyField(Player, blank=True)

     def __str__(self):
        return self.task_name

     def get_result(self):
        return ", ".join([str(r) for r in self.result.all()])
     get_result.short_description = "Result"


class Game(models.Model):
      game_id = models.AutoField('ID', primary_key=True)
      game_name = models.CharField(max_length=100)
      players = models.ManyToManyField(Player, blank=True)
      game_tasks = models.ManyToManyField(Task, blank=True)

      def __str__(self):
        return "Game " + str(self.game_id)

      def get_players(self):
        return ", ".join([str(p) for p in self.players.all()])
      get_players.short_description = "Players"
    
      def get_tasks(self):
        return ", ".join([str(t) for t in self.game_tasks.all()])
      get_tasks.short_description = "Tasks"