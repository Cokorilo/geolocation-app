# Generated by Django 3.2.13 on 2022-07-02 18:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geolocation_app', '0004_alter_task_result'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='result',
            field=models.ManyToManyField(null=True, to='geolocation_app.Player'),
        ),
    ]
